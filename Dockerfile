ARG TERRAFORM_VERSION=0.12.12
FROM hashicorp/terraform:${TERRAFORM_VERSION}

ARG AWSCLI_VERSION=1.16.272
ARG JQ_VERSION=1.6-r0
ARG YQ_VERSION=2.4.1
RUN \
  apk add --no-cache \
    bash \
    py-pip \
    jq=${JQ_VERSION} \
    zip \
    curl \
    rsync && \
  pip install \
    awscli==${AWSCLI_VERSION} && \
  curl -fsSL -o /usr/bin/yq \
    https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 && \
  chmod 755 /usr/bin/yq

# Image args, defined at build time.
ARG BUILD_DATE
ARG NAME
ARG VCS_COMMIT
ARG VCS_URL

LABEL \
  ca.paullalonde.awscli-version="${AWSCLI_VERSION}" \
  ca.paullalonde.jq-version="${JQ_VERSION}" \
  ca.paullalonde.terraform-version="${TERRAFORM_VERSION}" \
  org.label-schema.build-date="${BUILD_DATE}" \
  org.label-schema.name="${NAME}" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.vcs-ref="${VCS_COMMIT}" \
  org.label-schema.vcs-url="${VCS_URL}" \
  org.label-schema.vendor="paullalonde"

ENTRYPOINT []
