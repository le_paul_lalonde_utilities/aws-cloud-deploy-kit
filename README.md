# aws-cloud-deploy-kit

Constructs a Docker image containing tools I use to deploy to the cloud.

The tools are:

* Terraform
* AWS CLI
* curl
* jq
* rsync
* zip

The built image is available on [Docker Hub](https://cloud.docker.com/repository/docker/paullalonde/aws-cloud-deploy-kit).
